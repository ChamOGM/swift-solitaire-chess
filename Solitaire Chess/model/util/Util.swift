//
//  Util.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 16/12/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa
/**
 * Classe utilitaire de l'application.
 * Elle permet notamment d'accéder au fichier progression.txt pour lire et écrire la progression.
 */
class Util: NSObject {
    let SAVE_FILE_NAME:String = "progression.txt"
    var url:URL?
    static var icon:IconSet = IconSet()
    
    
    override init() {
        super.init()
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            self.url = dir.appendingPathComponent(SAVE_FILE_NAME)
        }
    }
    
    func writeNewMaxLevel(level:String) {
        //writing
        do {
            try level.write(to: url!, atomically: false, encoding: .utf8)
        }
        catch {
            print("erreur dans text.write : ", error)
        }
    }
    
    func readMaxLevel() -> Int {
        //reading
        var value:String?
        do {
            value = try String(contentsOf: url!, encoding: .utf8)
        }
        catch {
            print("erreur dans read : ", error)
        }
        
        return Int(value!)!
    }
    
    /**
     * Fonction utilitaire qui récupère, pour une pièce donnée, l'image qui lui est associée.
     */
    static func getIcon(piece:Piece?) -> NSImage? {
        var iconP:NSImage?
        switch piece?.type {
        case .Pawn?:
            iconP = icon.whitePawn
        case .King?:
            iconP = icon.whiteKing
        case .Queen?:
            iconP = icon.whiteQueen
        case .Knight?:
            iconP = icon.whiteKnight
        case .Bishop?:
            iconP = icon.whiteBishop
        case .Rook?:
            iconP = icon.whiteRook
        default:
            iconP = nil
        }
        return iconP
    }
}
