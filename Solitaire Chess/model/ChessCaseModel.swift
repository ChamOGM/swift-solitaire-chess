//
//  ChessCaseModel.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class ChessCaseModel: NSObject, NSCopying {
    var piece:Piece?
    
    init(piece:Piece?) {
        self.piece = piece
    }
    
    override var description: String {
        if piece == nil {
            return " nil "
        }
        return "\(piece!.lig),\(piece!.col),\(String(describing:piece!.type))"
    }
    
    func copy(with zone: NSZone? = nil) -> Any
    {
        let model = ChessCaseModel.init(piece: self.piece?.copy() as? Piece)
        return model
    }
}
