//
//  Piece.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class Piece: NSObject, NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let p:Piece = Piece.init(lig: self.lig, col: self.col)
        p.type = self.type
        
        return p
    }
    
    var lig:Int
    var col:Int
    var type:PieceType?
    
    init(lig:Int, col:Int) {
        self.lig = lig
        self.col = col
    }
    
    func canMove(lig:Int, col:Int, piece:Piece?) -> Bool { return false }
    
    public enum PieceType {
        case Pawn
        case Rook
        case Knight
        case Bishop
        case Queen
        case King
    }
}
