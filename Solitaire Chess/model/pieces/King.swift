//
//  King.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class King: Piece {
    override init(lig: Int, col: Int) {
        super.init(lig:lig, col:col)
        type = PieceType.King
    }
    
    override func copy() -> Any {
        let king:King = King(lig:self.lig, col:self.col)
        
        return king
    }
    
    override func canMove(lig: Int, col: Int, piece: Piece?) -> Bool {
        if piece == nil || piece == self {
            return false
        }
        
        if (abs(lig - self.lig) > 1 || abs(col - self.col) > 1) {
            return false
        }
        
        return true
    }
}
