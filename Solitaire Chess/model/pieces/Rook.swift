//
//  Rook.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class Rook: Piece {
    override init(lig: Int, col: Int) {
        super.init(lig:lig, col:col)
        type = PieceType.Rook
    }
    
    override func copy() -> Any {
        
        let r:Rook = Rook(lig: self.lig, col: self.col)
        
        return r
    }
    
    override func canMove(lig: Int, col: Int, piece: Piece?) -> Bool {
        if piece == nil || piece == self {
            return false
        }
        
        if lig != self.lig && col != self.col {
            return false
        }
        
        return true
    }
}
