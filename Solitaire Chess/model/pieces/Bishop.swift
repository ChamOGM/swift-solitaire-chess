//
//  Bishop.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class Bishop: Piece {
    override init(lig: Int, col: Int) {
        super.init(lig:lig, col:col)
        type = PieceType.Bishop
    }
    
    override func copy() -> Any {
        let b:Bishop = Bishop(lig: self.lig, col: self.col)
        
        return b
    }
    
    override func canMove(lig: Int, col: Int, piece: Piece?) -> Bool {
        if piece == nil || piece == self {
            return false
        }
        
        if abs(self.lig - lig) != abs(self.col - col) {
            return false
        }
        
        return true
    }
}
