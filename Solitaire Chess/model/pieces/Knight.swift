//
//  Knight.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class Knight: Piece {
    override init(lig: Int, col: Int) {
        super.init(lig:lig, col:col)
        type = PieceType.Knight
    }
    
    override func copy() -> Any {
        let k:Knight = Knight(lig: self.lig, col: self.col)
        return k
    }
    
    override func canMove(lig: Int, col: Int, piece: Piece?) -> Bool {
        if piece == nil || piece == self {
            return false
        }
        
        if piece!.lig - self.lig == 0 || piece!.col - self.col == 0 {
            return false
        }
        
        if abs(piece!.lig - self.lig) == 1 && abs(piece!.col - self.col) == 2 || abs(piece!.lig - self.lig) == 2 && abs(piece!.col - self.col) == 1 {
            return true
        }
        
        return false
    }
}
