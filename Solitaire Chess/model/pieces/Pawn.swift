//
//  Pawn.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class Pawn: Piece {
    override init(lig: Int, col: Int) {
        super.init(lig:lig, col:col)
        type = PieceType.Pawn
    }
    
    override func copy() -> Any {
        let p:Pawn = Pawn(lig: self.lig, col: self.col)
        
        return p
    }
    
    override func canMove(lig: Int, col: Int, piece: Piece?) -> Bool {
        if piece == nil || piece == self {
            return false
        }
        if (lig > self.lig) {
            return false
        }
        
        if lig < self.lig - 1 {
            return false
        }
        
        if abs(col - self.col) != 1 {
            return false
        }
        
        return true
    }
}
