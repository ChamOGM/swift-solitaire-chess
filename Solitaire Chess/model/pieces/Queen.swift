//
//  Queen.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class Queen: Piece {
    override init(lig: Int, col: Int) {
        super.init(lig:lig, col:col)
        type = PieceType.Queen
    }
    
    override func copy() -> Any {
        let queen:Queen = Queen(lig: self.lig, col: self.col)
        
        return queen
    }
    
    override func canMove(lig: Int, col: Int, piece: Piece?) -> Bool {
        if piece == nil || piece == self {
            return false
        }
        
        // same line or same column but not on the same place
        if self.lig == lig || self.col == col && (self.lig != lig || self.col != col) {
            return true
        }
        
        if abs(self.lig - lig) != abs(self.col - col) {
            return false
        }
        
        return true
    }
}
