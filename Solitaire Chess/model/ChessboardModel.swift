//
//  ChessboardModel.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

/**
 * Classe qui définit le modèle de l'application, avec la liste des niveaux, le niveau courant, le niveau max atteint, ...
 */
class ChessboardModel: NSObject {
    
    var levels:[Chessboard] = []
    var currentLevel:Int = 0
    var maxLevel:Int = 0
    var observer:NSObject?
    var mapDiffLevel:[Chessboard.Difficulty:[Chessboard]] = [:]
    
    /**
     * Constructeur de la classe. Elle récupère tous les niveaux à partir d'un fichier et les enregistre dans le tableau.
     */
    init(filePath:String) {
        super.init()
        print(filePath)
        do {
            let file:Data = try Data.init(contentsOf: Bundle.main.url(forResource: filePath, withExtension: "")! )
            let attibutedString = try NSAttributedString(data: file, documentAttributes: nil)
            let fullText = attibutedString.string
            let lines = fullText.components(separatedBy: CharacterSet.newlines)
            var i:Int = 1
            for level in lines {
                let chess:Chessboard? = fillChessboard(chessText: level, levelNo:i)
                if (chess != nil) {
                    if (mapDiffLevel[chess!.difficulty!] == nil) {
                        mapDiffLevel[chess!.difficulty!] = []
                    }
                    mapDiffLevel[chess!.difficulty!]?.append(chess!)
                }
                i += 1
            }
            //print(fullText)
        } catch {
            print(error)
        }
    }
    
    override func addObserver(_ observer: NSObject, forKeyPath keyPath: String, options: NSKeyValueObservingOptions = [], context: UnsafeMutableRawPointer?) {
        self.observer = observer
    }
    
    func getChessboard() -> Chessboard {
        return levels[currentLevel]
    }
    
    /**
     * Méthode qui effectue la prise de piece.
     * lig représente la ligne d'arrivée,
     * col représente la colonne d'arrivée,
     * piece la pièce à bouger.
     */
    func doMove(lig:Int, col:Int, piece:Piece) {
        levels[currentLevel].doMove(lig: lig, col: col, piece: piece)
        levels[currentLevel].decreaseNbPieces()
        if levels[currentLevel].nbPieces == 1 {
            observer?.observeValue(forKeyPath: "nbPieces", of: levels[currentLevel].nbPieces, change: [NSKeyValueChangeKey.newKey : (Any).self], context: nil)
        }
    }
    
    func increaseLevel() {
        currentLevel += 1
        if maxLevel < currentLevel {
            print("dans if mL < cL")
            maxLevel += 1
        }
        observer?.observeValue(forKeyPath: "iLevel", of: currentLevel, change: [NSKeyValueChangeKey.newKey : (Any).self], context: nil)
    }
    
    func setLevel(newLevel:Int) {
        currentLevel = newLevel
        observer?.observeValue(forKeyPath: "iLevel", of: currentLevel, change: [NSKeyValueChangeKey.newKey : (Any).self], context: nil)
    }
    
    func restartLevel() {
        levels[currentLevel].restartLevel()
        observer?.observeValue(forKeyPath: "rLevel", of: currentLevel, change: [NSKeyValueChangeKey.newKey : (Any).self], context: nil)
    }
    
    func decreaseLevel() {
        currentLevel -= 1
        observer?.observeValue(forKeyPath: "dLevel", of: currentLevel, change: [NSKeyValueChangeKey.newKey : (Any).self], context: nil)
    }
    
    func setMaxLevel(maxLevel:Int) {
        self.maxLevel = maxLevel
    }
    
    /**
     * Fonction qui remplit le tableau de niveau avec le tableau passé sous forme textuelle, et le niveau à afficher à l'usager.
     */
    func fillChessboard(chessText:String, levelNo:Int) -> Chessboard? {
        var chessboard:[[ChessCaseModel]] = []
        var row = 0
        let totNb:Int = chessText.count - 2
        var result:Chessboard?
        if totNb > 0 {
            let width:Int = Int(sqrt(Double(totNb)))
            for _ in 0..<width  {
                chessboard.append([ChessCaseModel]())
            }
            var stringCut:Array = Array(chessText)
            var col:Int = 0
            var nbPieces:Int = 0
            for index in 0..<totNb {
                let char = stringCut[index]
                
                var piece:Piece?
                switch char {
                case "P":
                    piece = Pawn(lig:row, col:col)
                    nbPieces += 1
                case "E":
                    piece = nil
                case "K":
                    piece = King(lig:row, col:col)
                    nbPieces += 1
                case "Q":
                    piece = Queen(lig:row, col:col)
                    nbPieces += 1
                case "C":
                    piece = Knight(lig:row, col:col)
                    nbPieces += 1
                case "F":
                    piece = Bishop(lig:row, col:col)
                    nbPieces += 1
                case "T":
                    piece = Rook(lig:row, col:col)
                    nbPieces += 1
                default:
                    piece = nil
                }
                if char != "E" {
                    chessboard[row].append(ChessCaseModel(piece:piece))
                }
                col += 1
                
                if index != 0 && ((index + 1) % width) == 0 {
                    row += 1
                    col = 0
                }
            }
            
            result = Chessboard(chessboard: chessboard, nbPieces: nbPieces, levelNo:levelNo)
            var diff:Chessboard.Difficulty?
            switch stringCut[stringCut.count - 1]{
            case "I":
                diff = .Intermediate
            case "A":
                diff = .Advanced
            case "E":
                diff = .Expert
            default:
                diff = .Easy
            }
            result!.difficulty = diff
            levels.append(result!)
        }
        return result
    }
}
