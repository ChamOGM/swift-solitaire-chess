//
//  Chessboard.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 25/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

/**
 * Classe qui représente un échiquier.
 */
class Chessboard: NSObject {
    var chessboard:[[ChessCaseModel]]?
    var baseChessboard:[[ChessCaseModel]]?
    
    var nbPieces:Int = 0
    var totNbPieces:Int = 0
    var difficulty:Difficulty?
    var levelNo:Int = 0
    
    init(chessboard:[[ChessCaseModel]], nbPieces:Int, difficulty:Difficulty, levelNo:Int) {
        self.chessboard = chessboard
        self.nbPieces = nbPieces
        self.totNbPieces = nbPieces
        self.baseChessboard = []
        var i:Int = 0
        for line in self.chessboard! {
            self.baseChessboard?.append([ChessCaseModel]())
            for c in line {
                self.baseChessboard![i].append(c.copy() as! ChessCaseModel)
            }
            i += 1
        }
        self.levelNo = levelNo
    }
    
    convenience init(chessboard:[[ChessCaseModel]], nbPieces:Int, levelNo:Int) {
        self.init(chessboard: chessboard, nbPieces: nbPieces, difficulty: Difficulty.Easy, levelNo:levelNo)
    }
    
    func doMove(lig:Int, col:Int, piece:Piece) {
        chessboard![piece.lig][piece.col].piece = nil
        chessboard![lig][col].piece = piece
        piece.lig = lig
        piece.col = col
    }
    
    func decreaseNbPieces() {
        nbPieces -= 1
    }
    
    func restartLevel() {
        for i in 0..<self.baseChessboard!.count {
            for j in 0..<self.baseChessboard![0].count {
                self.chessboard![i][j] = self.baseChessboard?[i][j].copy() as! ChessCaseModel
            }
        }
        self.nbPieces = self.totNbPieces
    }
    
    public enum Difficulty {
        case Easy
        case Intermediate
        case Advanced
        case Expert
    }
}
