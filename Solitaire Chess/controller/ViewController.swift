//
//  ViewController.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 17/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

/**
 * Contrôleur premier de l'application. C'est ici que la gestion du défilement des pages est faite,
 * ainsi que la sauvegarde de l'application
 */
class ViewController: NSPageController, NSPageControllerDelegate {
    
    
    
    @IBOutlet weak var playButton: NSButton!
    var controllers:[NSViewController]?
    var levelChooserWindow:ChooseLevelWindowController?
    var saveReaderWriter:Util?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let array = ["one", "two"]
        controllers = []
        self.delegate = self
        self.arrangedObjects = array
        self.transitionStyle = .horizontalStrip
    }
    
    
    /**
     * Méthode qui est appelée lorsqu'on clique sur le menuItem "Choose Level".
     * Il instancie le contrôleur de la fenêtre s'il n'existe pas encore, et l'affiche ensuite.
     */
    @IBAction func showChooseMenuView(_ sender:AnyObject) {
        let chessVC:ChessboardViewController = controllers![1] as! ChessboardViewController
        if controllers?.count == 2 {
            levelChooserWindow = NSStoryboard.init(name: "Main", bundle: nil).instantiateController(withIdentifier: "ChooseLevelWindowID") as? ChooseLevelWindowController
            controllers?.append(levelChooserWindow!.contentViewController!)
            let chooseLevelVC:ChooseLevelViewController = controllers![2] as! ChooseLevelViewController
            chooseLevelVC.setModel(model: chessVC.model!)
        }
        levelChooserWindow!.showWindow(self)
    }
    
    /**
     * Méthode qui est appelée lorsque l'on ferme l'application proprement (Cmd + Q ou "Quitter").
     *
     * Elle sauvegarde le niveau maximum atteint et termine l'application.
     */
    @IBAction func saveAndQuit(_ sender:AnyObject) {
        print("On va terminer l'appli !")
        let chessVC:ChessboardViewController = controllers![1] as! ChessboardViewController
    
        let text = String(chessVC.model!.maxLevel)
        saveReaderWriter?.writeNewMaxLevel(level: text)
    
        NSApp.terminate(nil)
    }
    
    
    /**
     * Cette méthode est appelée automatiquement par l'application lors de la configuration.
     * Elle va créer les contrôleurs, associer un modèle si nécessaire, et les enregistrer dans un tableau de NSViewController pour récupérer les
     * informations intéressantes lors de la sauvegarde de la progression.
     *
     */
    func pageController(_ pageController: NSPageController, viewControllerForIdentifier identifier: String) -> NSViewController {
        var c:NSViewController?
        if self.saveReaderWriter == nil {
            self.saveReaderWriter = Util()
        }
        switch identifier {
        case "one":
            c = NSStoryboard(name: "Main", bundle:nil).instantiateController(withIdentifier: "AccueilID") as? NSViewController
            controllers?.append(c!)
            return c!
        case "two":
            c = NSStoryboard(name: "Main", bundle:nil).instantiateController(withIdentifier: "ChessboardID") as? ChessboardViewController
            controllers?.append(c!)
            (c as! ChessboardViewController).setModel(model:ChessboardModel(filePath:"levels.strings"))
            (c as! ChessboardViewController).model!.setMaxLevel(maxLevel: (saveReaderWriter?.readMaxLevel())!)
            return c!
        /*case "three":
            return NSStoryboard(name: "Main", bundle:nil).instantiateController(withIdentifier: "Page03") as! NSViewController
         */
        default:
            return self.storyboard?.instantiateController(withIdentifier: identifier) as! NSViewController
        }
    }
    
    
    func pageController(_ pageController: NSPageController, identifierFor object: Any) -> String {
        return String(describing: object)
        
    }
    
    func pageControllerDidEndLiveTransition(_ pageController: NSPageController) {
        self.completeTransition()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
}

