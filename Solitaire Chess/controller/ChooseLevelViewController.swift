//
//  ChooseLevelViewController.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 26/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

/**
 * Classe qui permet de sélectionner un niveau parmi ceux déjà réussis.
 *
 */
class ChooseLevelViewController: NSViewController, NSComboBoxDataSource {
    
    var model:ChessboardModel?
    var selected:Chessboard?
    var v:ChooseLevelView?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        v = self.view as? ChooseLevelView
        v?.chooseLevelCB.usesDataSource = true
        v?.chooseLevelCB.dataSource = self
    }
    
    func setModel(model:ChessboardModel) {
        self.model = model
        v!.chooseLevelCB.completes = false
        
        v!.chooseLevelCB.reloadData()
        
    }
    
    /**
     * Cette méthode est appelée lorsqu'on clique sur le bouton "Valider" après avoir choisi un niveau.
     * Il définit le niveau nouvellement sélectionné dans le modèle, qui mettra ensuite à jour l'affichage de l'observer.
     */
    @IBAction func goToLevel(_ sender: Any) {
        let levelChosen:Int = v!.chooseLevelCB.indexOfSelectedItem
        print("niveau choisi ! \(levelChosen)")
        if levelChosen > -1 {
            model?.setLevel(newLevel: levelChosen)
        }
    }
    
    
    /**
     * Fonction qui retourne le niveau max atteint, et 0 si le modèle n'est pas initialisé.
     */
    func numberOfItems(in comboBox: NSComboBox) -> Int {
        return model?.maxLevel ?? 0//.count ?? 0
    }
    
    // Returns the object that corresponds to the item at the specified index in the combo box
    func comboBox(_ comboBox: NSComboBox, objectValueForItemAt index: Int) -> Any? {
        return index + 1
    }
    
}
