//
//  ChessboardViewController.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

/**
 * Contrôleur principal de l'application. Il gère la page qui contient l'échiquier.
 * Cette classe met en place un système d'observation de son modèle et de sa vue afin de mettre à jour l'UI lors de changement dans le modèle,
 * et de changer le modèle lors d'action dans la fenêtre.
 */
class ChessboardViewController: NSViewController {
    
    var model:ChessboardModel?
    var boardView:ChessboardView?
    var icon:IconSet = IconSet()
    var app:NSApplication = NSApplication.shared
    public let WIDTH:Int = 70
    public let DISTANCE_FROM_LEFT:Int = 60
    
    /**
     * Met à jour le modèle du contrôleur et ajoute un observer dessus, sur le nombre de pièces.
     */
    func setModel(model:ChessboardModel) {
        self.model = model
        self.model?.addObserver(self, forKeyPath: "nbPieces", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    /**
     * Classe qui est appelée une fois la page apparue. Elle rajoute tous les observers sur la vue, dessine l'échiquier et met en place les écouteurs
     * sur les boutons "Quitter" et "Sélectionner niveau".
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        boardView = (self.view as! ChessboardView)
        boardView?.addObserver(self, forKeyPath: "clickOnPiece", options: NSKeyValueObservingOptions.old, context: nil)
        var isBlack = true
        var row = 0
        let count:Int = model?.getChessboard().chessboard![0].count ?? 0
        for _ in 0..<count {
            boardView?.cases.append([ChessCase]())
        }
        for cases in (model?.getChessboard().chessboard)! {
            var col = 0
            for boardCase in cases {
                let iconP:NSImage? = Util.getIcon(piece: boardCase.piece)
                boardView?.cases[row].append(ChessCase(xPixels:(col) * WIDTH + DISTANCE_FROM_LEFT, yPixels:(count - row) * WIDTH + 2, modelP:boardCase, isBlackCase:isBlack, iconP:iconP))
                isBlack = !isBlack
                col += 1
            }
            row += 1
            isBlack = !isBlack
        }
        
        boardView?.levelNoLabel.stringValue = String(model!.getChessboard().levelNo)
        
        manageButtonsAvailability()
        
        let solChessMenu:NSMenu! = app.mainMenu!.item(withTitle: "Solitaire Chess")!.submenu!
        let quitButton:NSMenuItem = solChessMenu.items[solChessMenu.items.count - 1]
        quitButton.action = #selector (ViewController.saveAndQuit(_:))
        
        let fileMenu:NSMenu! = app.mainMenu!.item(withTitle: "Fichier")!.submenu!
        let chooseLevel: NSMenuItem = fileMenu.items[0]
        chooseLevel.action = #selector (ViewController.showChooseMenuView(_:))
    }
    
    /**
     * Méthode qui met à jour la possibilité pour l'usager de cliquer sur les boutons de changement de niveau.
     *
     */
    func manageButtonsAvailability() {
        if model?.currentLevel == model?.maxLevel {
            boardView?.nextLevelButton.isEnabled = false
        } else {
            boardView?.nextLevelButton.isEnabled = true
        }
        
        if model?.currentLevel == 0 {
            boardView?.lastLevelButton.isEnabled = false
        } else {
            boardView?.lastLevelButton.isEnabled = true
        }
    }
    
    /**
     * Méthode appelée automatiquement lorsqu'un changement est notifié dans la vue ou le modèle. Elle fait le lien entre ces deux derniers.
     *
     */
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        print("je rentre dans observe value !")
        print(keyPath!, "     ", change!)
        
        // Lors d'un clic sur une pièce, regarde si le mouvement est possible, et le fait le cas échéant, en mettant à jour le modèle.
        if keyPath!.elementsEqual("clickOnPiece") {
            let obj:[ChessCase?] = object as! [ChessCase?]
            if (obj[1]?.model?.piece != nil && obj[0]?.model?.piece != nil) {
                attemptPieceMove(pieces:obj)
                boardView?.selected = nil
                obj[1]?.fillColor = (obj[1]?.isBlackCase)! ? NSColor.darkGray : NSColor.white
                obj[0]?.fillColor = (obj[0]?.isBlackCase)! ? NSColor.darkGray : NSColor.white
            } else if obj[1]?.model?.piece != nil {
                obj[1]?.fillColor = NSColor.green
                
            } else if obj[1]?.model?.piece == nil {
                obj[0]?.fillColor = (obj[0]?.isBlackCase)! ? NSColor.darkGray : NSColor.white
            }
            return
        }
        
        manageButtonsAvailability()
        
        // Si on rentre ici alors c'est qu'il ne reste qu'une pièce. On met à jour la disponibilité du bouton "Niveau suivant".
        if keyPath!.elementsEqual("nbPieces") {
            boardView?.nextLevelButton.isEnabled = true
        } else {
            // Si on rentre ici c'est que l'utilisateur choisit de changer de niveau.
            if keyPath!.elementsEqual("iLevel") || keyPath!.elementsEqual("dLevel") {
                
                var cases:[[ChessCase]] = boardView!.getChessCases()
                let chessboard:Chessboard = model!.getChessboard()
                for i in 0..<cases[0].count {
                    for j in 0..<cases[0].count {
                        cases[i][j].model = chessboard.chessboard![i][j]
                        cases[i][j].icon = Util.getIcon(piece: chessboard.chessboard![i][j].piece)
                    }
                }
                
                boardView?.levelNoLabel.stringValue = String(model!.getChessboard().levelNo)
            } else if keyPath!.elementsEqual("rLevel") {
                // Si on rentre ici c'est que l'utilisateur choisit de réinitialiser le niveau courant.
                var cases:[[ChessCase]] = boardView!.getChessCases()
                boardView?.selected = nil
                let chessboard:Chessboard = model!.getChessboard()
                
                for i in 0..<cases[0].count {
                    for j in 0..<cases[0].count {
                        cases[i][j].model = chessboard.chessboard![i][j]
                        cases[i][j].icon = Util.getIcon(piece: chessboard.chessboard![i][j].piece)
                    }
                }
            }
            // Dans tous les cas, on rafraichit la vue.
            boardView?.setNeedsDisplay((boardView?.visibleRect)!)
        }
        
    }
    
    /**
     * Fonction qui vérifie si le déplacement est valide, et le fait le cas échéant.
     */
    func attemptPieceMove(pieces:[ChessCase?]) {
        let pieceBeg : Piece? = pieces[0]?.model?.piece
        print("pieceBeg : ", pieceBeg as Any)
        let pieceEnd : Piece? = pieces[1]?.model?.piece
        print("pieceEnd : ", pieceEnd as Any)
        
        //if pieceBeg != nil && pieceEnd != nil {
            if pieceBeg!.canMove(lig: pieceEnd!.lig, col: pieceEnd!.col, piece: pieceEnd) {
                print("Il peut le manger !")
                let oldLig:Int = pieceBeg!.lig
                let oldCol:Int = pieceBeg!.col
                model?.doMove(lig: pieceEnd!.lig, col: pieceEnd!.col, piece: pieceBeg!)
                
                var cases:[[ChessCase]] = boardView!.getChessCases()
                cases[oldLig][oldCol].icon = Util.getIcon(piece: nil)
                cases[pieceEnd!.lig][pieceEnd!.col].icon = Util.getIcon(piece: pieceBeg)
                boardView?.setNeedsDisplay((boardView?.visibleRect)!)
            }
        //}
    
    }
    
    // Fonctions qui modifient dans le modèle le niveau courant (pour aller au précédent, le réinitialiser, ou aller au suivant)
    @IBAction func lastLevel(_ sender: Any) {
        model!.decreaseLevel()
    }
    
    @IBAction func restartLevel(_ sender: Any) {
        model!.restartLevel()
    }
    @IBAction func nextLevel(_ sender: Any) {
        model!.increaseLevel()
    }
}
