//
//  ChooseLevelWindowController.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 09/12/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class ChooseLevelWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

}
