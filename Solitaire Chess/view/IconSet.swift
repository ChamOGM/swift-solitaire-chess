//
//  IconSet.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 30/12/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

struct IconSet {
    var whitePawn: NSImage!
    var whiteRook: NSImage!
    var whiteKnight: NSImage!
    var whiteBishop: NSImage!
    var whiteQueen: NSImage!
    var whiteKing: NSImage!
    
    init(){
        whitePawn = NSImage(named: "normal_white_pawn")
        whiteRook = NSImage(named: "normal_white_rook")
        whiteKnight = NSImage(named: "normal_white_knight")
        whiteBishop = NSImage(named: "normal_white_bishop")
        whiteQueen = NSImage(named: "normal_white_queen")
        whiteKing = NSImage(named: "normal_white_king")
    }
}
