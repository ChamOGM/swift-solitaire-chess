//
//  ChessCase.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

/**
 * Classe qui représente une case de l'échiquier.
 */
class ChessCase: NSBox {
    var model:ChessCaseModel?
    var icon:NSImage?
    var isBlackCase:Bool?
    
    public static let WIDTH:Int = 71
    
    init(xPixels:Int, yPixels:Int, modelP:ChessCaseModel, isBlackCase:Bool, iconP:NSImage?) {
        super.init(frame: NSMakeRect(CGFloat(xPixels), CGFloat(yPixels), CGFloat(ChessCase.WIDTH), CGFloat(ChessCase.WIDTH)))
        self.boxType = NSBox.BoxType.custom
        self.borderType = NSBorderType.noBorder
        self.isBlackCase = isBlackCase
        if isBlackCase {
            fillColor = NSColor.darkGray
        } else {
            fillColor = NSColor.white
        }
        
        self.model = modelP
        
        icon = iconP
        if icon != nil  && (icon?.isValid)!{
            let imageView:NSImageView = NSImageView(image:icon!)
            
            //imageView.image = icon
            contentView?.addSubview(imageView)
            
        }
    }
    
    func setModel(model:ChessCaseModel?) {
        self.model = model
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        self.icon?.draw(in: dirtyRect)
    }
    
    
    required init?(coder decoder: NSCoder) {
        fatalError("ChessCase.init(decoder) not implemented")
    }
    
    
}
