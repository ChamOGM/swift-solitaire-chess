//
//  ChooseLevelView.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 30/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa

class ChooseLevelView: NSView {
    
    @IBOutlet weak var chooseLevelCB: NSComboBox!
    
    @IBOutlet weak var validateLevelButton: NSButton!
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        // Drawing code here.
    }
}
