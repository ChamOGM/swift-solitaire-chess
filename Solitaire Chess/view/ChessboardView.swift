//
//  ChessboardView.swift
//  Solitaire Chess
//
//  Created by Gabriel Pellerin on 18/11/2018.
//  Copyright © 2018 Gabriel Pellerin. All rights reserved.
//

import Cocoa
/**
 * Classe qui contient la vue de l'échiquier, ainsi que des boutons de sélection de niveau et du label d'affichage de niveau courant.
 *
 */
class ChessboardView: NSView {
    
    @IBOutlet weak var nextLevelButton: NSButton!
    @IBOutlet weak var lastLevelButton: NSButton!
    @IBOutlet weak var restartButton: NSButton!
    @IBOutlet weak var levelNoLabel: NSTextField!
    var cases = [[ChessCase]]()
    var selected:ChessCase?
    var observer:NSObject?
    
    required init?(coder decoder: NSCoder) {
        super.init(coder:decoder)
    }
    
    func getChessCases() -> [[ChessCase]] {
        return cases
    }
    
    /**
     * Méthode qui ajoute dans la vue les cases de l'échiquier.
     */
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        // Drawing code here.
        for lines in cases {
            for caseBox in lines {
                addSubview(caseBox)
            }
        }
    }
    
    override func addObserver(_ observer: NSObject, forKeyPath keyPath: String, options: NSKeyValueObservingOptions = [], context: UnsafeMutableRawPointer?) {
        self.observer = observer
    }
    
    /**
     * Méthode qui est appelée à la fin d'un clic de l'usager.
     * Elle récupère la case choisie et la transmet à son observer pour traitement.
     */
    override func mouseUp(with event: NSEvent) {
        let coord:NSPoint = event.locationInWindow
        let line:Int = 3 - (Int(coord.y) - 72) / ChessCase.WIDTH
        let column:Int = (Int(coord.x) - 60) / ChessCase.WIDTH
        if (line < 4 && line >= 0 && column < 4 && column >= 0) {
            let oldValue:ChessCase? = self.selected
            self.selected = cases[line][column]
            
            print("nouveau click hehe")
            observer?.observeValue(forKeyPath: "clickOnPiece", of: [oldValue, self.selected], change: [NSKeyValueChangeKey.oldKey : (Any).self], context: nil)
        }
    }
}
